PZI CATALOGUE 2011
==================

Design of the graduation flyer and catalogue of MA Networked Media of the Piet Zwart Institute, Rotterdam.

Bound with removeable black brads, the catalogue is composed of one stapled introduction booklet, followed by 6 leaflet/posters, one for each student. The purple poster ink seems to be absorbed by the red leaflet side. Each booklet and leaflet can be distributed individually.

We used public domain patent drawings to illustrate each project with a shift of humour.

-----

Software: GNU/Linux, Scribus, Gimp, Inkscape, ImageMagick, Fontforge, PodofoImpose...
Fonts: Univers Else and Crickx.
Printed at Cassochrome (BE).
